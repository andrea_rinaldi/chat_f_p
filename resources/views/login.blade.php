@extends('layouts.app')
@section('header')
    <link href="{{secure_asset('css/login.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
            <div class="form-group">
                <div class="col-md-12 login-content">
                    <div class="col-md-5 login-center-5 login-content">
                        <img  style="width:60%;" src="{{secure_asset('img/login_circle.png')}}">
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
        {!! Form::open(['url' => secure_url('/login'), 'method'=>'POST', 'id'=>'login_form','enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('session_id',null,['id' => 'session_id']) !!}
            <div class="form-group">
                <div class="col-md-12 login-content">
                    <div class="col-md-5 login-center-5 login-content">
                        {!! Form::label('nickname','Nickname:') !!}
                        {!! Form::text('nickname',null,['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
            <div class="form-group">
                <div class="col-md-12 login-content">
                    <div class="col-md-5 login-center-5 login-content">
                        {!! Form::label('language','Lingua:') !!}
                        {!! Form::select('language', Config::get('constants.languages'),null,['class' => 'form-control ']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
            <div class="form-group" id="pass_form_row" style="display:none">
                <div class="col-md-12 login-content">
                    <div class="col-md-5 login-center-5 login-content">
                        {!! Form::label('password','Password:') !!}
                        {!! Form::password('password',['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
            <div class="form-group">
                <div class="col-md-12 login-content">
                    <div class="col-md-5 login-sub-content login-content">
                        <input type="button" class="btn btn-outline-default" value="Enter" id="login_submit_get" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row" >
        <div class="col-md-12 login-sub-content" style="display: none">
            <button class="btn-outline-info" id="toggle_pass">Pass. Toggle</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('footer')
    <script src="{{secure_asset('js/login.js')}}"></script>
@endsection

