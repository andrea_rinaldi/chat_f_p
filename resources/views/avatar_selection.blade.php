@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-5  avatar_title">
                <h1> Choose an Avatar </h1>
            </div>
        </div>
        {!! Form::open(['url' => url('/avatar/{session_id}')  , 'method'=>'POST', 'id'=>'avatar_form','enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('session_id',null,['id' => 'session_id']) !!}
        {!! Form::hidden('avatar_id',1,['id' => 'avatar_id']) !!}
        <div class="row">
            <div class="col-md-12 avatar_table">
                <?php $counter=1?>
                <table width="50%">
                    <tr>
                        <th style="visibility: hidden"></th>
                        <th style="visibility: hidden"></th>
                        <th style="visibility: hidden"></th>
                        <th style="visibility: hidden"></th>
                        <th style="visibility: hidden"></th>
                        <th style="visibility: hidden"></th>
                    </tr>
                    @foreach($avatars as $avatar)
                        @if($counter%6 == 1)
                            <tr>
                                @endif
                                <td>
                                    <img data-content="{{$avatar->id}}" style="width:60%; border:4px solid white;" class="avatar_img" src="{{\Config::get('constants.avatar_path.path') . $avatar->filename}}">
                                </td>
                                @if($counter%6 == 0)
                            </tr>
                        @endif
                        <?php $counter++ ?>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-5  avatar_choose_button">
                {!! Form::submit('Choose',['id' => 'avatar_choice' ,'class' => 'btn btn-outline-default']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('footer')
    <script src="{{secure_asset('js/avatar.js')}}"></script>
@endsection
