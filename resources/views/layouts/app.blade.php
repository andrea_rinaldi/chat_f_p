<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>PF_Chat</title>

        <link href="{{secure_asset('vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{secure_asset('css/app.css')}}" rel="stylesheet">
        <link href="{{secure_asset('css/dev.css')}}" rel="stylesheet">
        @yield('header')
    </head>

    <body>

        @yield('content')

    </body>

    <footer>
        <script src="{{secure_asset('vendors/jquery/jquery.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="{{secure_asset('vendors/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{secure_asset('vendors/jquery/jquery.cookie.js')}}"></script>
        <script src="{{secure_asset('js/utility.js')}}"></script>
        @yield('footer')
    </footer>
</html>

