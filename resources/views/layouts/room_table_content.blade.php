<div class="room row">
    <div class="rooms col-md-5">{{ $room->name }}</div>
    <div class="rooms col-md-2">{{ $room->filled }}/{{ $room->size }}</div>
    <div class="rooms col-md-3">{{ $room->hostName }}</div>
    <div class="rooms col-md-2">{{ $room->languageName() }}</div>
</div>