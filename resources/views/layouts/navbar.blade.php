<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">PF_Chat</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active lobby_menu_item">
                    <a class="nav-link" href="#">Admin Page</a>
                </li>
                <li class="nav-item lobby_menu_item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#CreateRoomModal">Create Room</a>
                </li>
                <li class="nav-item lobby_menu_item">
                    <a id="return_to_avatar_sel" class="nav-link" href="#">Return to Avatar Selection</a>
                </li>
            </ul>
            <ul class="navbar-nav  lobby_menu_logout">
                <li class="nav-item">
                    <a id="logout_btn" class="nav-link" href="#">Logout</a>
                </li>
                <li class="nav-item navbar_avatar_container">
                    <img class="navbar_avatar" src="{{\Config::get('constants.avatar_path.path') . $chosen_avatar_name}}">
                </li>
            </ul>
        </div>
    </div>
</nav>