<div id="CreateRoomModal" class="modal fade" role="dialog">
    <div class="modal-dialog lobby_model_dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create a Room</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => url('/rooms/create') ,'id' => 'room_form' , 'method'=>'POST','enctype' => 'multipart/form-data', 'files'=>'true']) !!}
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                {{Form::label('name', 'Room Name:')}}
                                {{Form::text('name',null,['class'=>'form-control'])}}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('size','Max Users:') !!}
                                {!! Form::select('size', ['0' => 0, '1' => 1],null,['class' => 'form-control ']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('language','Language:') !!}
                                {!! Form::select('language', Config::get('constants.languages'),null,['class' => 'form-control ']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('hosting_type','Hosting Type:') !!}
                                {!! Form::select('hosting_type', Config::get('constants.hosting_types'),null,['class' => 'form-control ']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" >
                            <div class="col-md-6 checkbox">
                                <label><input type="checkbox" name="room_pass" value="Bike" id="password_toggle"> Set a Password</label>
                            </div>
                            <div class="col-md-6" id="room_pass_row" style="display:none">
                                {!! Form::label('password','Password:') !!}
                                {!! Form::password('password',['class' => 'form-control', 'id' => 'room_password']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group group-submit-btn">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4 modal_create_room_submit">
                                {{Form::submit('Crea',['class' => 'btn btn-success', 'style' => 'width:100px;'])}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
     </div>
</div>