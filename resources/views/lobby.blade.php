@extends('layouts.app')

@section('header')
    <link href="{{secure_asset('css/lobby.css')}}" rel="stylesheet">
@endsection
@include('modals.create_room')

@section('content')
    <div class="col-md-12">
        <div class="container">
            <div class="upper row">
                <div class="col-md-12">
                    @include('layouts.navbar')
                </div>
            </div>

            <div class="mid row">
                <div class="col-md-12">
                    <div class="rooms col-md-6">
                        <div class="rooms panel col-md-12" style="border-color:{{ '#'.$color }}">
                            @include('layouts.room_table_header', ["color"=>$color])
                            <div class="panel-body">
                                @foreach($rooms as $room)
                                    @include('layouts.room_table_content', ["room"=>$room])
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="rooms col-md-6">
                        <div class="row">
                            @include('layouts.lobby_menu', ["color"=>$color])
                        </div>
                        <div class="row">
                            <div class="premiums panel col-md-12" style="border-color:{{ '#'.$color }}">
                                @include('layouts.room_table_header')
                                <div class="panel-body">
                                    @foreach($premiums as $premium)
                                        @include('layouts.room_table_content', ["room"=>$premium])
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="lower row">

            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script src="{{secure_asset('js/lobby.js')}}"></script>
@endsection