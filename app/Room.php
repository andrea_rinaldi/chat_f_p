<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Illuminate\Support\Facades\DB;

class Room extends Model
{
    protected $table = 'rooms';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'host_id',
        'name',
        'size',
        'password',
        'premium',
        'language',
        'created_at',
        'updated_at'
    ];

    public function getHost(){
        $raw = "SELECT *
                  FROM users
                  WHERE users.id = ?;";
        $host = DB::select($raw, [$this->host_id]);
        if(isset($host)) return $host[0];
        else return null;
    }

    public function getHostName(){
        $host = $this->getHost();
        if($host != null) return $host->nickname;
        else return 'democracy';
    }

    public function getUsers(){
        $raw = "SELECT *
                  FROM room_user
                  LEFT JOIN users ON room_user.user_id = users.id
                  WHERE room_user.room_id = ?;";
        $users = DB::select($raw, [$this->id]);
        $host = $this->getHost();
        if($host != null) array_push($users, $host);
        return $users;
    }

    public function languageName(){
        return Config::get('constants.languages.'.$this->language);
    }
}
