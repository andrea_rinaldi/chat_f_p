<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use PhpParser\ErrorHandler\Collecting;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'avatar_id',
        'session_id',
        'nickname',
        'language',
        'online',
        'created_at',
        'updated_at'
    ];

    public function getAvatar(){
        $raw = "SELECT *
                  FROM avatars
                  WHERE avatars.id = ?;";
        $host = DB::select($raw, [$this->avatar_id]);
        return $host[0];
    }

    public function getActiveRooms(){
        $raw = "SELECT * FROM room_user WHERE user_id = ? AND ghost = 0;";
        $active_rooms_raw = DB::select($raw, [$this->id]);
        $active_rooms = [];
        foreach($active_rooms_raw as $active_room_raw){
            $active_room = new RoomUser($active_room_raw);
            array_push($active_rooms, $active_room);
        }

        return $active_rooms;
    }


}
