<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $table = 'avatars';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'filename',
        'color'
    ];
}
