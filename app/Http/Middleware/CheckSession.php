<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session_id = $request->session_id;
        $check = User::where("session_id", $session_id)->get()->first();
        if(!isset($check)) return redirect('/');
        $request->request->add(["user" => $check]);
        return $next($request);
    }
}
