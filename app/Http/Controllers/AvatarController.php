<?php

namespace App\Http\Controllers;

use App\Avatar;
use Illuminate\Http\Request;

class AvatarController extends Controller
{
    public function page(){
        $avatars = Avatar::all();

        return view('avatar_selection', compact('avatars'));
    }

    public function pick(Request $request){
        $user = $request->input("user");
        $avatar_id = $request->input("avatar_id");
        $avatar = Avatar::where("id", $avatar_id)->get()->first();
        try{
            $user->avatar_id = $avatar->id;
            $user->online = true;
            $user->save();

            return redirect("/lobby/".$user->session_id);

        }catch(\Exception $e){
            return redirect("/avatar/".$user->session_id);
        }
    }
}
