<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DebugController extends Controller
{
    public function cryptedPwd(){
        $prima = "vanilla01";

        $prima = password_hash($prima, PASSWORD_DEFAULT);

        return view('puntobladepuntopiaccapi', compact('prima'));
    }
}
