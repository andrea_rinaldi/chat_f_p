<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Utils\RandomStringGenerator;

class LoginController extends Controller
{
    public function page(){
        return view('login');
    }

    public function ajaxLogin(Request $request){ //ajax call for login

        $nickname = $request->input("nickname");
        $password = $request->input("password");
        $language = $request->input("language");
        $old = User::where("nickname", $nickname)->leftJoin("registered_users", "users.id", "=", "registered_users.user_id")->get()->first();

        $canLog = true;
        if(isset($old)){
            if($old->online){ //another user is logged with the same nickname
                $canLog = false;
                $message = [
                    "type" => "error",
                    "code" => "nickname_not_available",
                    "verbose" => "another user is already using that nickname, please pick another one",
                ];
                return json_encode($message);
            }
            elseif(isset($old->password)){ //the nickname is registered
                if($password != ""){
                    if (!password_verify($password, $old->password)) { //wrong password
                        $canLog = false;
                        $message = [
                            "type" => "error",
                            "code" => "wrong_password",
                            "verbose" => "there is no user registered with this nickname and/or password",
                        ];
                        return json_encode($message);
                    }
                }
                else { //request password
                    $canLog = false;
                    $message = [
                        "type" => "trigger",
                        "code" => "password_required",
                        "verbose" => "you need a password to login with that nickname",
                    ];
                    return json_encode($message);
                }
            }
        }

        if($canLog){ //create new session_id
            $session_id = RandomStringGenerator::generate(20);
            try{
                if(!isset($old)){
                    $new = [
                        'avatar_id' => null,
                        'session_id' => "TEMP",
                        'nickname' => $nickname,
                        'language' => $language,
                        'online' => false,
                    ];
                    $old = User::create($new);
                }

                $old = User::where("nickname", $nickname)->get()->first();
                $old->session_id = $session_id;
                $old->save();

                $message = [
                    "type" => "success",
                    "code" => "session_id",
                    "verbose" => $session_id,
                ];

            }catch(Exception $e){
                $message = [
                    "type" => "error",
                    "code" => "session_id",
                    "verbose" => "cannot create a session id, please retry in 5 seconds",
                ];
            }

            return json_encode($message);
        }

    }

    public function login(Request $request){ //login with sessison_id

        $session_id = $request->input("session_id");
        $user = User::where("session_id", $session_id)->get()->first();

        if(isset($user)){
            return redirect("/avatar/".$session_id);
        }
        else{
            return redirect("/");
        }
    }
}
