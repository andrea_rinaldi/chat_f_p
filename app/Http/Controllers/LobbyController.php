<?php

namespace App\Http\Controllers;

use App\Room;
use App\RoomUser;
use Illuminate\Http\Request;

class LobbyController extends Controller
{
    public function page(Request $request){

        $rooms = Room::where("premium", false)->get();
        foreach($rooms as $room){
            $room->filled = count($room->getUsers());
            $room->hostName = $room->getHostName();
        }
        $premiums = Room::where("premium", true)->get();
        foreach($premiums as $room){
            $room->filled = count($room->getUsers());
            $room->hostName = $room->getHostName();
        }
        $user = $request->input("user");
        $color = $user->getAvatar()->color;
        $chosen_avatar_name = $user->getAvatar()->filename;

        return view('lobby', compact('rooms', 'premiums', 'user', 'color', 'chosen_avatar_name'));
    }

    public function create(){
        //the user wants to create a room
    }

    public function join(){
        //the user wants to join a room
    }

    public function logout(Request $request){
        //Segnare l'utente come offline
        //Segnarlo fantastama nella stanza in cui si trovava al momento del logout
        //Redirect alla pagina di login
        $user = $request->input("user");
        $user->online = 0;
        $user->save();

        $active_rooms = $user->getActiveRooms();
        foreach($active_rooms as $active_room){
            $active_room->ghost = 1;
            $active_room->save();
        }

        return redirect('/');


    }




}
