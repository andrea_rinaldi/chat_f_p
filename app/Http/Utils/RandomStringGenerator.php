<?php

namespace App\Http\Utils;

class RandomStringGenerator{

    public static function generate($size = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $len = strlen($characters);
        $randstring = '';
        for ($i = 0; $i < $size; $i++) {
            $randstring .= $characters[rand(0, $len - 1)];
        }
        return $randstring;
    }

}