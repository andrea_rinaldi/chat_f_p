<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredUser extends Model
{
    protected $table = 'registered_users';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'user_id',
        'password',
        'created_at',
        'updated_at'
    ];
}
