$(document).ready(function(){

    /***************************Login Ajax Request***********************************/
    $('#login_submit_get').on('click',function(e){
        e.preventDefault();

        var user_nick = $('#nickname').val();
        var user_pass = $('#password').val();
        var user_language = $('#language').val();

        $.ajax({
            type: 'POST',
            url: '/ajax/login',
            dataType: 'json',
            data: {
                'nickname': user_nick,
                'password': user_pass,
                '_token': $('meta[name=csrf-token]').attr('content'),
                'language': user_language
            }
        }).done(function(data){
            switch(data.type){
                case 'nickname_not_available':
                    alert(data.verbose);
                    break;
                case 'trigger':
                    $('#pass_form_row').fadeIn(700);
                    console.log(data.verbose);
                    break;
                case 'success':
                    //Setting the cookie
                    $.cookie('session_id', data.verbose, {
                        expires:2,
                        path: '/',
                        domain:'pfchat.it',
                        secure:false
                    });

                    console.log($.cookie('session_id'));


                    //Set the session id val
                    $('#session_id').val(data.verbose);
                    $('#login_form').submit();
                    break;
                case 'error':
                    alert(data.verbose);
                    break;
            }
        }).fail(function(){
            console.log('Errore Login');
        });
    });

    //Clicking on Enter. On the keyboard
    $(document).keypress(function(e){
        if (e.which == 13){
            $("#login_submit_get").click();
        }
    });



});