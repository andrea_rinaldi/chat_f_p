$(document).ready(function(){
    /*************************** Setting the values of the avatar post form ***********************************/
    $('#session_id').val($.cookie('session_id'));
    $('#avatar_form').attr('action','/avatar/' + $.cookie('session_id'));

    $('.avatar_img').on('click',function(){
        var avatar_id = $(this).attr('data-content');
        //Cleaning other images borders
        $('.avatar_img').each(function(){
            $(this).css('border', '4px solid #ffffff');
        });
        //Focusing on the choose image
        $(this).css('border', '4px solid #ffc107');
        $('#avatar_id').val(avatar_id);
    });


    //Clicking on Enter. On the keyboard
    $(document).keypress(function(e){
        if (e.which == 13){
            $("#avatar_choice").click();
        }
    });
});