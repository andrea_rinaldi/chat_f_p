$(document).ready(function(){
    //Change Return to Avatar Selection Button's Href
    $('#return_to_avatar_sel').attr('href','/avatar/' + $.cookie('session_id'));
    //Change Logout HREF on page load
    $('#logout_btn').attr('href','/logout/' + $.cookie('session_id'));


    //Toggling password input in create room modal
    $('#password_toggle').on('click', function(e){
        e.preventDefault();
        $('#room_pass_row').toggle("medium");

        if($("#password_toggle").attr('checked') === false){
            $("#password_toggle").attr('checked', true);
        }else{
            $("#password_toggle").attr('checked', false);
            $('#room_password').val('');
        }
    });

    //Setting session_id to null on logout
    $('#logout_btn').on('click',function(e){
        e.preventDefault();
        $.removeCookie('session_id');
        console.log('Logout');
        // e.run();
    });

});
