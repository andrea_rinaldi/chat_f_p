<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/dev/debug", "DebugController@cryptedPwd");

// LOGIN
Route::get("/", "LoginController@page");
Route::post("/ajax/login", "LoginController@ajaxLogin");
Route::post("/login", "LoginController@login");

// AVATAR
Route::get("/avatar/{session_id}", "AvatarController@page")->middleware("check_session");
Route::post("/avatar/{session_id}", "AvatarController@pick")->middleware("check_session");

// LOBBY
Route::get("/lobby/{session_id}", "LobbyController@page")->middleware("check_session");
Route::get("/logout/{session_id}", "LobbyController@logout")->middleware("check_session");

// ROOMS
