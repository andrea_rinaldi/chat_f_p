<?php

namespace Tests\Feature;

use App\Admin;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DatabaseTest extends TestCase
{
    /**A basic test example.*/
    public function testAdminsTable()
    {
        $success = true;
        try{
            $newAdmin = [];
            Admin::create($newAdmin);
        }catch(Exception $e){
            $success = false;
        }
        $this->assertFalse($success); //should not create an admin without user_id
        $success = true;
        try{
            $newAdmin = [
                "user_id" => -1
            ];
            Admin::create($newAdmin);
        }catch(Exception $e){
            $success = false;
        }
        $this->assertFalse($success); //should not create an admin with user_id < 0
        $success = true;

    }

    /**A basic test example.*/
    public function testAvatarsTable()
    {
        $this->assertTrue(true);
    }

    /**A basic test example.*/
    public function testInviteCodesTable()
    {
        $this->assertTrue(true);
    }

    /**A basic test example.*/
    public function testMessagesTable()
    {
        $this->assertTrue(true);
    }

    /**A basic test example.*/
    public function testRegisteredUsersTable()
    {
        $this->assertTrue(true);
    }

    /**A basic test example.*/
    public function testRoomsTable()
    {
        $this->assertTrue(true);
    }

    /**A basic test example.*/
    public function testUsersTable()
    {
        $this->assertTrue(true);
    }
}
